package cat.itb.m13.tdboard.model.repositoris;

import cat.itb.m13.tdboard.model.entitats.Task;
import org.springframework.data.jpa.repository.JpaRepository;


public interface TaskRepository extends JpaRepository<Task, Integer> { }
