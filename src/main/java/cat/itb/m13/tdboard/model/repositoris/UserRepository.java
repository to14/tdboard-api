package cat.itb.m13.tdboard.model.repositoris;

import cat.itb.m13.tdboard.model.entitats.Usuari;
import org.springframework.data.jpa.repository.JpaRepository;


public interface UserRepository extends JpaRepository<Usuari, Integer> {
}
