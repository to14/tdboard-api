package cat.itb.m13.tdboard.controladors;

import cat.itb.m13.tdboard.model.entitats.Lista;
import cat.itb.m13.tdboard.model.entitats.Task;
import cat.itb.m13.tdboard.model.entitats.Usuari;
import cat.itb.m13.tdboard.model.serveis.ServeisItem;
import cat.itb.m13.tdboard.model.serveis.ServeisLista;
import cat.itb.m13.tdboard.model.serveis.ServeisUser;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class ControladorTDBoard {

    private final ServeisItem serveisItem;
    private final ServeisLista serveisLista;
    private final ServeisUser serveisUser;


    //USER
    @GetMapping("/todousers")
    public List<Usuari> getUsers() {
        return serveisUser.get();
    }
    @GetMapping("/todousers/{id}")
    public ResponseEntity<Usuari> consultarUsuari(@PathVariable Integer id) {
        Usuari user = serveisUser.getById(id);
        if (user == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(user);
    }
    @PostMapping("/todousers")
    public ResponseEntity<Usuari> postUser(@RequestBody Usuari user) {
        Usuari u = serveisUser.set(user);
        return new ResponseEntity<Usuari>(u, HttpStatus.CREATED);
    }
    @PutMapping("/todousers")
    public ResponseEntity<Usuari> updateUser(@RequestBody Usuari user) {
        Usuari u = serveisUser.set(user);
        return ResponseEntity.ok(u);
    }
    @DeleteMapping("/todousers/{id}")
    public Usuari deleteUser(@PathVariable int id) {
        return serveisUser.delete(id);
    }

    //LLISTA
    @GetMapping("/todolists")
    public ResponseEntity<List<Lista>> getLists() {
        return ResponseEntity.ok(serveisLista.get());
    }
    @GetMapping("/todolists/{id}")
    public ResponseEntity<Lista> getListsById(@PathVariable int id){
        Lista lista = serveisLista.getById(id);
        if(lista == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(lista);
    }
    @PostMapping("/todolists")
    public ResponseEntity<Lista> postLista(@RequestBody Lista lista){
        Lista l = serveisLista.set(lista);
        return new ResponseEntity<Lista>(l, HttpStatus.CREATED);
    }
    @PutMapping("/todolists")
    public ResponseEntity<Lista> updateLista(@RequestBody Lista lista) {
        Lista l = serveisLista.set(lista);
        return ResponseEntity.ok(l);
    }
    @DeleteMapping("/todolists/{id}")
    public ResponseEntity<Integer> deleteList(@PathVariable int id) {
        serveisLista.delete(id);
        return new ResponseEntity<>(id, HttpStatus.NO_CONTENT);
    }

    @GetMapping("/todolist/user/{id}")
    public ResponseEntity<List<Lista>> getListByUserId(@PathVariable int id) {
        List<Lista> lista = serveisLista.getListByUserId(id);
        return ResponseEntity.ok(lista);
    }

    //ITEM
    @GetMapping ("/todoitems")
    public ResponseEntity<List<Task>> getTasks(){
        return ResponseEntity.ok(serveisItem.get());
    }
    @GetMapping("/todoitems/{id}")
    public ResponseEntity<Task> getTaskById(@PathVariable int id){
        Task t = serveisItem.getById(id);
        if (t == null) return ResponseEntity.notFound().build();
        return ResponseEntity.ok(t);
    }
    @PostMapping("/todoitems")
    public ResponseEntity<Task> postTask (@RequestBody Task task){
        Task t = serveisItem.set(task);
        return new ResponseEntity<>(t, HttpStatus.CREATED);
    }
    @PutMapping("/todoitems")
    public ResponseEntity<Task> putTask (@RequestBody Task task) {
        Task t = serveisItem.set(task);
        return ResponseEntity.ok(t);
    }
    @DeleteMapping("/todoitems/{id}")
    public ResponseEntity<Integer> deleteTask (@PathVariable int id){
        serveisItem.delete(id);
        return new ResponseEntity<>(id, HttpStatus.NO_CONTENT);
    }
}
