package cat.itb.m13.tdboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TdBoardApplication {

    public static void main(String[] args) {
        SpringApplication.run(TdBoardApplication.class, args);
    }

}
