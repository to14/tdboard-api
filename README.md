# DOCUMENTACIÓ API TDBOARD
##  USERS

+ `GET /todousers` obté la llista de tots els usuaris
+ `GET /todousers/:id` obté l'usuari per id
+ `POST /todousers` + `body: {id: 1, ... }` afegeix usuaris
+ `PUT /todousers` + `body: {id: 1, ... }` actualitza un usuari
+ `DELETE /todousers/:id` borra un usuari

## LISTS
+ `GET /todolists` obté la llista de tots els llistas
+ `GET /todolists/:id` obté la llista per id
+ `POST /todolists` + `body: {id: 1, ... }` afegeix llistas
+ `PUT /todolists` + `body: {id: 1, ... }` actualitza una llista
+ `DELETE /todolists/:id` borra un llista

## TASK
+ `GET /todoitems` obté la tasca de tots els tasques
+ `GET /todoitems/:id` obté la tasca per id
+ `POST /todoitems` + `body: {id: 1, ... }` afegeix tasques
+ `PUT /todoitems` + `body: {id: 1, ... }` actualitza una tasca
+ `DELETE /todoitems/:id` borra un tasca
